<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;


/**
 * @ORM\Entity(repositoryClass="App\Repository\WineRepository")
 * @ApiResource
 */
class Wine
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $domainName;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $cuveName;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\CavernWine", mappedBy="wine", orphanRemoval=true)
     */
    private $cavernWines;

    public function __construct()
    {
        $this->cavernWines = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getDomainName(): ?string
    {
        return $this->domainName;
    }

    public function setDomainName(string $domainName): self
    {
        $this->domainName = $domainName;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCuveName()
    {
        return $this->cuveName;
    }

    /**
     * @param mixed $cuveName
     *
     * @return Wine
     */
    public function setCuveName($cuveName): self
    {
        $this->cuveName = $cuveName;

        return $this;
    }

    /**
     * @return Collection|CavernWine[]
     */
    public function getCavernWines(): Collection
    {
        return $this->cavernWines;
    }

    public function addCavernWine(CavernWine $cavernWine): self
    {
        if (!$this->cavernWines->contains($cavernWine)) {
            $this->cavernWines[] = $cavernWine;
            $cavernWine->setCavern($this);
        }

        return $this;
    }

    public function removeCavernWine(CavernWine $cavernWine): self
    {
        if ($this->cavernWines->contains($cavernWine)) {
            $this->cavernWines->removeElement($cavernWine);
            // set the owning side to null (unless already changed)
            if ($cavernWine->getCavern() === $this) {
                $cavernWine->setCavern(null);
            }
        }

        return $this;
    }
}
