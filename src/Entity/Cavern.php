<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiSubresource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CavernRepository")
 * @UniqueEntity(
 *     fields={"name"},
 *     message="Ce nom est déjà utilisée"
 * )
 * @ApiResource
 */
class Cavern
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(name="name", type="string", length=255, unique=true)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\CavernWine", mappedBy="cavern", orphanRemoval=true)
     * @ApiSubresource
     */
    private $cavernWines;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="caverns")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    public function __construct()
    {
        $this->cavernWines = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function __toString()
    {
        return $this->name;
    }

    /**
     * @return Collection|CavernWine[]
     */
    public function getCavernWines(): Collection
    {
        return $this->cavernWines;
    }

    public function addCavernWine(CavernWine $cavernWine): self
    {
        if (!$this->cavernWines->contains($cavernWine)) {
            $this->cavernWines[] = $cavernWine;
            $cavernWine->setCavern($this);
        }

        return $this;
    }

    public function removeCavernWine(CavernWine $cavernWine): self
    {
        if ($this->cavernWines->contains($cavernWine)) {
            $this->cavernWines->removeElement($cavernWine);
            // set the owning side to null (unless already changed)
            if ($cavernWine->getCavern() === $this) {
                $cavernWine->setCavern(null);
            }
        }

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }
}
