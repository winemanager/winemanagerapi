<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass="App\Repository\CavernWineRepository")
 */
class CavernWine
{

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Cavern", inversedBy="cavernWines")
     * @ORM\JoinColumn(nullable=false)
     */
    private $cavern;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Wine", inversedBy="cavernWines")
     * @ORM\JoinColumn(nullable=false)
     */
    private $wine;

    /**
     * @ORM\Column(type="integer")
     */
    private $quantity;

    /**
     * @ORM\Column(type="integer")
     */
    private $year;

    public function getId()
    {
        return $this->id;
    }

    public function getCavern(): ?Cavern
    {
        return $this->cavern;
    }

    public function setCavern(?Cavern $cavern): self
    {
        $this->cavern = $cavern;

        return $this;
    }

    public function getWine(): ?Wine
    {
        return $this->wine;
    }

    public function setWine(?Wine $wine): self
    {
        $this->wine = $wine;

        return $this;
    }

    public function getQuantity(): ?int
    {
        return $this->quantity;
    }

    public function setQuantity(int $quantity): self
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * @param mixed $y@ear
     *
     * @return CavernWine
     */
    public function setYear($year): self
    {
        $this->year = $year;

        return $this;
    }
}
