<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 */
class User implements UserInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=25, unique=true)
     *
     * @Assert\NotBlank()
     * @Assert\Unique()
     */
    private $username;

    /**
     * @ORM\Column(type="string", length=25, unique=true)
     *
     * @Assert\NotBlank()
     * @Assert\Unique()
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=500)
     *
     * @Assert\NotBlank()
     * @Assert\Unique()
     */
    private $password;

    /**
     * @ORM\Column(name="is_active", type="boolean")
     */
    private $isActive;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Cavern", mappedBy="user", orphanRemoval=true)
     */
    private $caverns;

    public function __construct()
    {
        $this->caverns = new ArrayCollection();
        $this->isActive = false;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUsername()
    {
        return $this->username;
    }

    public function setUsername(?string $username): self
    {
        $this->username = $username;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getRoles()
    {
        return array('ROLE_USER');
    }

    public function eraseCredentials()
    {
    }

    public function getSalt()
    {
        return null;
    }

    /**
     * @return Collection|Cavern[]
     */
    public function getCaverns(): Collection
    {
        return $this->caverns;
    }

    public function addCavern(Cavern $cavern): self
    {
        if (!$this->caverns->contains($cavern)) {
            $this->caverns[] = $cavern;
            $cavern->setUser($this);
        }

        return $this;
    }

    public function removeCavern(Cavern $cavern): self
    {
        if ($this->caverns->contains($cavern)) {
            $this->caverns->removeElement($cavern);
            // set the owning side to null (unless already changed)
            if ($cavern->getUser() === $this) {
                $cavern->setUser(null);
            }
        }

        return $this;
    }
}
