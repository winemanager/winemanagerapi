<?php

namespace App\Repository;

use App\Entity\Cavern;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Cavern|null find($id, $lockMode = null, $lockVersion = null)
 * @method Cavern|null findOneBy(array $criteria, array $orderBy = null)
 * @method Cavern[]    findAll()
 * @method Cavern[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CavernRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Cavern::class);
    }


//    /**
//     * @return Cavern[] Returns an array of Cavern objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Cavern
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
