<?php

namespace App\Repository;

use App\Entity\CavernWine;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method CavernWine|null find($id, $lockMode = null, $lockVersion = null)
 * @method CavernWine|null findOneBy(array $criteria, array $orderBy = null)
 * @method CavernWine[]    findAll()
 * @method CavernWine[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CavernWineRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, CavernWine::class);
    }

//    /**
//     * @return CavernWine[] Returns an array of CavernWine objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CavernWine
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
